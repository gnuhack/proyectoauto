#+LANGUAGE: es
#+AUTHOR: Jorge Rdodr�guez Rubio, Carlos Pajuelo Rojo
#+TITLE: GNUS: REPARTO DE PRENSA

* Visualizaci�n principal
#+CAPTION: Esquema del almac�n del proyecto.
#+NAME:   fig:Visualizaci�n
[[./vis.png]]
* Descripci�n del sistema

El proyecto resuelve el problema de log�stica y tratamiento del producto gracias a un almac�n automatizado de reparto de prensa. El flujo de trabajo empieza en la cinta principal. En primer lugar, entran paquetes por la cinta, que son m�s tarde escaneados por un lector de c�digos QR para poder clasificarse posteriormente. Seguidamente, al llegar a la plataforma giratoria con rodillos, dependiendo del tipo de paquete se traslada a la cinta A, B o C, donde se le aplican sendos procesos seg�n el tipo de paquete (encartado, flejado o embolsado). Para mejorar la eficiencia del sistema, decidimos posteriormente a�adir cintas A2, B2 y C2 despu�s de los procesos. Tras estas cintas son llevados a una cinta com�n (cinta intermedia). Al final de esta cinta se encuentra un cruce (recurso compartido) donde se decide qu� paquete pasa en funci�n de un orden de prioridad previamente asignado (es prioritario el paquete B, luego el A y por �ltimo el C). Al final de la cinta intermedia un brazo mec�nico se encargar� de trasladar los paquetes a las cintas finales (cintas 1, 2 y 3) que conectan con los respectivos almacenes (A, B y C). Desde aqu� tres AGV ir�n cogiendo los tres tipos de paquetes. Cada AGV tiene asignado un solo tipo de paquete, que luego ir� depositando en los tres camiones dependiendo de los pedidos de cada cami�n.

* Objetivos de control

  El objetivo es que los paquetes que van entrando por la cinta principal cada 5 segundos acaben cargados en los camiones de acuerdo a los pedidos que �stos realicen. La finalidad ser�a que el control funcionara de forma ininterrumpida ante un suministro ilimitado de paquetes, pero, debido a las limitaciones de Codesys, hemos decidido crear solo 5 paquetes a modo de demostraci�n.

* Modos de funcionamiento

  El modo de funcionamiento por defecto es el modo autom�tico, en el que todo el proceso desde que aparece el paquete en la cinta principal hasta la carga en el cami�n es autom�tico. Por otra parte tenemos el modo manual, a trav�s del cual se pueden manejar todos los actuadores de la planta con la interfaz HMI, implementada en la POU visual  "Botonera_general". Por �ltimo hemos decidido implementar un modo "pausa" que, a diferencia del marcha/paro, deja la planta paralizada en caso de que estuviera operando en modo autom�tico hasta que se desactive el modo. Tiene prioridad sobre marcha.

* Explicaci�n de la visualizaci�n

  Hemos decidido implementar los sensores con forma triangular, excepto uno con forma cuadrada el cual representa que la plataforma giratoria est� completamente girada. Adem�s tenemos un c�digo de colores dual. Cuando los sensoresest�n desactivados se muestran en rojo, y al activarse pasan a estar coloreados en verde hasta que se vuelven a desactivar. Por otra parte hemos incluido indicadores visuales que permiten ver de forma f�cil qu� cintas o procesos est�n activados en cada momento. Estos indicadores se muestran en blanco cuando su actuador correspondiente est� desactivado y en azul en caso contrario. Tambi�n tenemos un sem�foro en el cruce de las cintas A, B y C con la cinta intermedia, que permite ver si el cruce est� o no ocupado.

  Por otro lado tenemos la botonera para el modo manual. En "Botonera_general" est�n los botones que manejan el almac�n, los AGV, todas las cintas y el brazo. A la izquierda tenemos los botones que controlan todas las cintas hasta llegar al brazo. En la parte de la derecha encontramos el control del brazo, de las cintas de los almacenes y de los AGV. En lo referente al control de los AGV, el bot�n "MARCHA" activa simult�neamente los tres AGV, que empiezan a recorrer el cirtcuito preestablecido.

  La "Botonera_pedidos" se encarga de gestionar los pedidos. Se pueden realizar pedidos de los tres tipos de paquetes a cada cami�n. Cuando el pedido est� listo se pulsa el bot�n "PEDIDO" para que los AGV se pongan en marcha una vez el pedido haya llegado al almac�n.

  En "ModosFunc" tenemos implementados los modos de funcionamiento. Un selector permite elegir el modo que queremos activar (AUTO o MANUAL), cuyo indicador se ilumina en verde cuando est� activo. Por otra parte tenemos los botones de MARCHA/PARO, y adem�s hemos a�adido el modo PAUSA/CONTINUE, que tiene prioridad sobre el modo AUTO. Cada uno de estos cuatro botones cuenta con una se�al luminosa que indica cuando est�n activos (verde), o en el caso de que AUTO se encuentre pausado se muestra el indicador de color �mbar.

 A continuaci�n, explicamos brevemente el funcionamiento de cada bot�n de la "Botonera_general":
 
*) Paquete:
 Permite soltar paquetes sobre la cinta principal en el modo manual.

*) C. ppal:
 Actuador de la cinta principal.

*) ROD_CARGA:
 Activa el actuador que permite la carga del paquete en la plataforma giratoria y su posterior descarga en la cinta B.

*) DESC_IZQ y DESC_DRCH:
 Activa los actuadores que permiten la descarga del paquete en las cintas A y C.

*) GIRO_CARGA:
 Actuador del giro de la plataforma giratoria en sentido antihorario.

*) GIRO_DESC:
 Actuador del giro de la plataforma giratoria en sentido horario.

*) C.A1, C.B1, C.C1:
 Actuadores del primer tramo de las cintas A, B y C.

*) C.A2, C.B2, C.C2:
 Actuadores del segundo tramo de las cintas A, B y C.

*) C. Int:
 Actuador de la cinta intermedia.

*) V:
 Actuador de retorno del brazo rob�tico.

*) A, B, C:
 Actuadores que permiten mover el brazo hasta las cintas 1, 2 y 3.

*) C1, C2, C3:
 Actuadores de las cintas 1, 2 y 3.

*) MARCHA:
 Activa o desactiva los tres AGV simult�neamente.

