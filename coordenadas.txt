* Coordenadas de las cintas

(x,y) (Coordenadas l�mites del centro del paquete):

Superior de la cinta principal:
(810-810, 1680-2150)
(0, -470)

Entrada de la cinta A:

(680-810, 1540-2150)
(-130, -610)

Inferior de la cinta A:
(390-810, 1540-2150)
(-420, -610)

Superior de la cinta A:
(390-810, 760-2150)
(-420, -1390)

Final de la cinta A:
(750-810, 760-2150)
(-60, -1390)

Entrada de la cinta B:
(810-810, 1400-2150)
(0, -750)

Final de la cinta B:
(810-810, 840-810)
(0, 30)

Entrada de la cinta C:
(940-810, 1540-2150)
(130, -610)

Superior de la cinta C:
(810-810, 830-2150)
(0, -1320)

Final de la cinta C:
(880-810, 760-2150)
(70, -1390)

Cinta intermedia izquierda:
(810-810, 270-2150)
(0, -1180)

Cinta intermedia derecha:
(2270-810, 270-2150)
(1460, -1180)

Almac�n A1:
(2040-810, 840-2150)
(1230, -1310)

Almac�n A2:
(2040-810, 1200-2150)
(1230, -950)

Almac�n A3:
(1870-810, 1200-2150)
(1060, -950)

Almac�n B1:
(2260-810, 840-2150)
(1690, -1310)

Almac�n B2:
(2260-810, 1200-2150)
(1450, -950)

Almac�n C1:
 (2500-810, 840-2150)
 (1230, -950)

Almac�n C2:
(2500-810, 1200-2150)
(1690, -950)

Almac�n C3:
(2670-810, 1200-2150)
(1860, -950)


* Coordenadas de los AGV:
  Coordenadas relativas (con la resta de las coordenadas iniciales ya realizada)

  AGV 1 carga:
  (1040, -550)

  AGV 2 carga:
  (1410, -550)

  AGV 3 carga:
  (1800, -550)

  C1:
  (1830, 60)

  C2:
  (1440, 60)

  C3:
  (1070, 60)
  
